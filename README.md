# FLL Masterpiece Griffin

This is the Git repository where Team-CERC-1 will have all the GRIFFIN robot's mission codes for the FLL Masterpiece 2023-24 season. This is our fail safe option.

### Robots
- CERC Team 1 has two options: 
    (1) MAIN ROBOT new design whose code lives in a different repository] (https://gitlab.com/cerc1/fll-masterpiece-2023-24/)
    (2) Griffin (fail safe option for 2023-24) [Main robot's code is in this repository](https://gitlab.com/cerc1/fll-masterpiece-griffin)
- We will have Griffin (our FLL SuperPowered 2022-23 robot) as the ‘default’ robot, and trial alternative designs to be our MAIN ROBOT. We will use the BETA Pybricks version, as it will become a stable version soon.
- The code for the GRIFFIN (OUR DEFAULT ROBOT) will reside in a separate repository. Griffin is kept as a fail-safe option

### Usage instructions
1.	Each team member should create their own gitlab account (if you have not already created one for the FLL SuperPowered or a previous season). 
2.	Coach will send gitlab invites to you to join the project (and create a new account simultaneously if you do not have one already). Check for emails in your inbox (and the spam folder). 
3.	The repository will have a permanent Git ‘branch’ where the team will update their code.
4.	We will use VSCode. Clone this repository and start creating your own .py file.
5.	Add a python virtual environment (works in Windows). Ctrl-Shift-P > Python: Create Environment
6.	pip install pybricks==3.3.0a5
7.	pip install pybricksdev
8.	Create a new python file, named teamMember-Test-mission.py, copy and paste the code below, and save it.
9.	Commit the changes, and push. It will probably prompt for gitlab registration/login and then sync all files. This link may help: https://pages.nist.gov/git-novice-MSE/08-collab/
10.	Install pybricks on each robot at https://beta.pybricks.com/. Avoid spaces and special characters in the robot name. 

### Running the program

There are two ways to run the code:

**Method I: Using the browser and Pybricks website**

1.	Open the website: beta.pybricks.org 
2.	On the top tab, clock the ‘Bluetooth’ button and connect to the robot (switch on the robot power button as well)
3.	On the top left corner, use the ‘import a file’ option (click the upward-pointing arrow icon) and choose -ALL- the files in the project folder
4.	Click ‘Run’ icon to run the program of your choice. 

**Method 2: Copy the program manually to the hub and run**

1.	Edit .vscode\tasks.json to run pybricksdev.exe, which uploads programs to the Spike hub. https://github.com/pybricks/pybricksdev The only changes needed should be to change the label and command argument to match the robot's name.
2.	Turn the robot on and ensure the keyboard shortcut ctrl-h runs the command, which should also run their program. Also, Ctrl-Shift-P > Tasks: Run task should pop up a menu with the correct entry.


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/cerc1/fll-masterpiece-griffin.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/cerc1/fll-masterpiece-griffin/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
